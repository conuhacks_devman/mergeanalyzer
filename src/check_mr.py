import gitlab
from dotenv import load_dotenv
import os
import requests
from llama import completion, stream_completion

load_dotenv()

PRIVATE_TOKEN = os.getenv("PRIVATE_TOKEN")

PROJECT_NAME_WITH_NAMESPACE = "conuhacks_devman/demo-testing/cal.com"

BOT_ID = 19759134

def filter_unresponded_mrs(mrs):
	# filter out the merge requests that have already been responded to by the ai
	# return the merge requests that have not been responded to by the ai
	unresponded_mrs = mrs
	for mr in mrs:
		notes = mr.notes.list()
		# if the last note is from the ai, then the ai has responded to the merge request
		for note in notes:
			if note.author["id"] == BOT_ID:
				unresponded_mrs.remove(mr)
				break

	return unresponded_mrs


def prepare_prompt(mr):
	# get the title
	# get the commits
	# get the diffs
	# prepare the prompt for the ai to respond to

	mr_title = mr.title

	mr_description = mr.description

	mr_sb = mr.source_branch

	mr_labels = mr.labels

	mr_commits = mr.commits()

	#prompt = "I am going to give you a merge request and its details, if there are suggestions to be made, " \
	#"you should give feedback consisting of helpful tips on good coding practices, logical issue, " + \
	#"syntax errors, etc in relation to the changes that have been made.\n"

	prompt = "The following is a pull request to a public repository on GitLab. The merge request will consist of a series of commits and their diffs, which are changes made by the user.\n" +\
		"The commits and diffs will be followed by suggestions in which a professional code reviewer provides helpful tips on good coding practices, logic issues, " +\
	 			"syntax errors, etc all in relation to the changes that can be seen in the commits.\n"
	
	prompt += f"\n# Title \n{mr_title}\n\n# Description \n{mr_description}" \
	f"\n\n# Branch \n{mr_sb}  \n\n# labels \n{mr_labels} \n\n# Commits\n" 

	for index, commit in enumerate(mr_commits):
		title = commit.title
		diffs = commit.diff()

		prompt += f"\nCommit number " + str(index + 1) + ". \"" + title + "\"\n"

		for i in range(len(diffs)):
			prompt += f"\nCommit diff number {i + 1}:\n" + str(diffs[i]['diff']) + "\n"


	# prompt = "The following is a merge request, followed by comments and suggestions by a code reviewer. The merge " + \
	# 	"request will consist of a series of commits and their diffs, which are changes made by the user. The comments " + \
	# 		"and suggestions by the code reviewer will consist of helpful tips on good coding practices, logic issues, " + \
	# 			"syntax errors, etc in relation to the changes that have been made. Here is the title of the merge " + \
	# 				"request: \"" + mr_title + "\"\nHere are the commits:\n"

	prompt += "\n# Suggestions:\n"

	return prompt

def respond_to_mr(mr):
	# respond to the merge request with the ai
	# make a note on the merge request with the response from the ai

	prompt = prepare_prompt(mr)

	print("prompt:\n" + prompt)

	url = "https://ai.mregirouard.com/completion"

	response = completion(url, prompt)

	print("response:\n" + response)

	mr.notes.create({"body": response})

if __name__ == "__main__":
	# get the project
	# get the merge requests
	# filter out the merge requests that have already been responded to by the ai
	# respond to the merge requests with the ai

	# get the project
	gl = gitlab.Gitlab("https://gitlab.com", private_token=PRIVATE_TOKEN)
	project = gl.projects.get(PROJECT_NAME_WITH_NAMESPACE)

	print("fetching merge requests...")

	# get the merge requests
	mrs = project.mergerequests.list(state="opened")

	# print(mrs[0])

	print("filtering merge requests...")

	# filter out the merge requests that have already been responded to by the ai
	unresponded_mrs = filter_unresponded_mrs(mrs)

	num_unresponded_mrs = len(unresponded_mrs)

	# respond to the merge requests with the ai
	for index, mr in enumerate(unresponded_mrs):
		print("responding to merge request #" + str(index + 1) + "...")
		respond_to_mr(mr)

	print("responded to " + str(num_unresponded_mrs) + " merge requests")
