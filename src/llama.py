import requests

# Run a completion on the Llama model
def completion(url: str, prompt: str, frequency_penalty: float = 0.0, grammar: str = "", microstat: float = 0.0,
	microstat_eta: float = 0.0, microstat_tau: float = 5, n_predict: int = 400, n_probs: int = 0,
	presence_penalty: float = 0.0, repeat_last_n: int = 0, repeat_penalty: float = 0.0,
	slot_id: int = -1, stop: list = ["</s>", "Llama:", "User:"], temperature: float = 0.7,
	tfs_z: int = 1, top_k: int = 40, top_p: float = 0.5, typical_p: int = 1) -> str:

	query = {
		"frequency_penalty": frequency_penalty,
		"grammar": grammar,
		"microstat": microstat,
		"microstat_eta": microstat_eta,
		"microstat_tau": microstat_tau,
		"n_predict": n_predict,
		"n_probs": n_probs,
		"presence_penalty": presence_penalty,
		"repeat_last_n": repeat_last_n,
		"repeat_penalty": repeat_penalty,
		"slot_id": slot_id,
		"stop": stop,
		"temperature": temperature,
		"tfs_z": tfs_z,
		"top_k": top_k,
		"top_p": top_p,
		"typical_p": typical_p,
		"prompt": prompt,
		"stream": False,
	}

	response = requests.post(url, json=query)
	return response.json()["content"]

# Run a completion on the Llama model, but stream the response
def stream_completion(url: str, prompt: str, frequency_penalty: float = 0.0, grammar: str = "", microstat: float = 0.0,
	microstat_eta: float = 0.0, microstat_tau: float = 5, n_predict: int = 400, n_probs: int = 0,
	presence_penalty: float = 0.0, repeat_last_n: int = 0, repeat_penalty: float = 0.0,
	slot_id: int = -1, stop: list = ["</s>", "Llama:", "User:"], temperature: float = 0.7,
	tfs_z: int = 1, top_k: int = 40, top_p: float = 0.5, typical_p: int = 1) -> requests.Response:

	query = {
		"frequency_penalty": frequency_penalty,
		"grammar": grammar,
		"microstat": microstat,
		"microstat_eta": microstat_eta,
		"microstat_tau": microstat_tau,
		"n_predict": n_predict,
		"n_probs": n_probs,
		"presence_penalty": presence_penalty,
		"repeat_last_n": repeat_last_n,
		"repeat_penalty": repeat_penalty,
		"slot_id": slot_id,
		"stop": stop,
		"temperature": temperature,
		"tfs_z": tfs_z,
		"top_k": top_k,
		"top_p": top_p,
		"typical_p": typical_p,
		"prompt": prompt,
		"stream": True,
	}

	session = requests.Session()
	return session.post(url, json=query, stream=True)
